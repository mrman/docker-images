# `backup-sidecar-postgres`

A sidecar container for running continuous backups on a [`postgres`][postgresql] instance.

## Versions

| Version            | Image URL                                                                          |
|--------------------|------------------------------------------------------------------------------------|
| `11.14-alpine3.15` | `registry.gitlab.com/mrman/docker-images/backup-sidecar-postgres:11.14-alpine3.15` |
| `13.4-alpine3.14`  | `registry.gitlab.com/mrman/docker-images/backup-sidecar-postgres:13.4-alpine3.14`  |
| `14.0-alpine3.14`  | `registry.gitlab.com/mrman/docker-images/backup-sidecar-postgres:14.0-alpine3.14`  |
| `14.5-alpine3.16`  | `registry.gitlab.com/mrman/docker-images/backup-sidecar-postgres:14.5-alpine3.16`  |

## Backup contents

`postgres` backups contain:

- Postgres backup (`pg_dumpall`)

## Configuration

| ENV                       | Default                          | Example                          | Description                                                                                        |
|---------------------------|----------------------------------|----------------------------------|----------------------------------------------------------------------------------------------------|
| `OPERATION`               | `backup`                         | `restore`                        | Operation to perform with the sidecar container                                                    |
| `BACKUP_NAME_PREFIX`      | `backup`                         | `my-special-backup`              | Prefix that will be used before date of backup                                                     |
| `BACKUP_NAME`             | `backup-$(date +%F@%H_%M_%S-%Z)` | `backup-2021-10-13@13_47_28-UTC` | Name of the backup file (`.tar.gz` will be added as a suffix, `$BUCKET__PREFIX` will be prepended) |
| `POSTGRES_DATA_DIR`       | `/data`                          | `/data`                          | Directory containing postgres data                                                                 |
| `PGUSER`                  | `postgres`                       | `postgres`                       | Postgres user that (used by `psql`)                                                                |
| `PGHOST`                  | N/A                              | `postgres`                       | Postgres host that (used by `psql`)                                                                |
| `PGPASSWORD`              | N/A                              | N/A                              | Password to use when connecting to postgres (used by `psql`)                                       |
| `BACKUP_SCRATCH_DIR`      | `/tmp`                           | `/backup`                        | Directory that will temporarily house backup                                                       |
| `BACKUP_INTERVAL_SECONDS` | `43200` (12h)                    | `86400` (24h)                    | Backup interval in seconds                                                                         |
| `BUCKET`                  | `backups`                        | `backups`                        | Top level bucket name                                                                              |
| `BUCKET_PREFIX`           | `postgres/backups`               | `your/dir/structure`             | Directory struture after bucket name, but before date (ex. `$BUCKET/$BUCKET_PREFIX/$BACKUP_NAME`)  |
| `B2_ACCOUNT_ID`           | N/A                              | N/A                              | [Backblaze B2][backblaze-b2] Account ID                                                            |
| `B2_KEY`                  | N/A                              | N/A                              | [Backblaze B2][backblaze-b2] Key                                                                   |

[postgresql]: https://postgresql.org
[backblaze-b2]: https://www.backblaze.com/b2
