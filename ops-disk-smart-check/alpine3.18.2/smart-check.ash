#!/usr/bin/env -S ash -euo pipefail

JQ=${JQ:-jq}
CURL=${CURL:-curl}
GREP=${GREP:-grep}
SMARTCTL=${SMARTCTL:-smartctl}

# Ensure smartctl comand exists
if [ -z "$(command -v ${SMARTCTL})" ]; then
  echo "smartctl is not installed!";
  exit 1;
fi

# Ensure jq comand exists
if [ -z "$(command -v ${JQ})" ]; then
  echo "jq is not installed!";
  exit 1;
fi

# Ensure grep comand exists
if [ -z "$(command -v ${GREP})" ]; then
  echo "grep is not installed!";
  exit 1;
fi

# Ensure WEBHOOK_URL was provided
WEBHOOK_URL=${WEBHOOK_URL:none}
if [ "$WEBHOOK_URL" == "none" ]; then
  echo "WEBHOOK_URL was not provided!";
  exit 1;
fi

# Ensure CLUSTER was provided
CLUSTER=${CLUSTER:none}
if [ "$CLUSTER" == "none" ]; then
  echo "CLUSTER was not provided!";
  exit 1;
fi

# Ensure NODE was provided
NODE=${NODE:none}
if [ "$NODE" == "none" ]; then
  echo "NODE was not provided!";
  exit 1;
fi

# Retrieve list of disks
DISK_PATHS=$(lsblk -o type,path,kname,fstype | grep disk | grep /nvme | /usr/bin/cut -d' ' -f3)
echo -e "[info] detected disk paths:\n$DISK_PATHS";
echo "";

# Check all the disks on this node
while read DISK_PATH; do
  echo -e "[info] looking at disk [$DISK_PATH]...";
  echo "";

  # Perform the SMART check on the disk
  SMART_OUTPUT=`${SMARTCTL} -a --test=short --json ${DISK_PATH}`

  # Pull out results
  SMART_PASSED=`echo $SMART_OUTPUT | ${JQ} '.smart_status.passed'`;
  SMART_AVAILABLE_SPARE=`echo $SMART_OUTPUT | ${JQ} '.nvme_smart_health_information_log.available_spare'`;
  SMART_AVAILABLE_SPARE_THRESHOLD=`echo $SMART_OUTPUT | ${JQ} '.nvme_smart_health_information_log.available_spare_threshold'`;
  SMART_PERCENTAGE_USED=`echo $SMART_OUTPUT | ${JQ} '.nvme_smart_health_information_log.percentage_used'`;

  echo -e "[info] output for disk [$DISK_PATH]:";
  echo -e "[info] nvme_smart_health_information_log.passed? [${SMART_PASSED}]";
  echo -e "[info] nvme_smart_health_information_log.availble_spare? [${SMART_AVAILABLE_SPARE}]";
  echo -e "[info] nvme_smart_health_information_log.available_spare_threshold? [${SMART_AVAILABLE_SPARE_THRESHOLD}]";
  echo -e "[info] nvme_smart_health_information_log.percentage_used? [${SMART_PERCENTAGE_USED}]";
  echo "";

  if [ "$SMART_PASSED" != "true" ]; then
      # If percentage_used >= 100 then SMART failures may happen despite the drive still being usable.
      #
      # if CHECK_SPARE_THRESHOLD is set, we can be more nuanced about the check,
      # knowing that the failure can be ignored if available_spare > threshold
      if [ "$SMART_PERCENTAGE_USED" -gt 99 ] && [ "$SMART_AVAILABLE_SPARE" -gt "$SMART_AVAILABLE_SPARE_THRESHOLD" ]; then
          echo -e "Disk check failed, but available_spare > threshold so we are safe.";
          exit 0;
      else
          echo -e "error detected on disk [$DISK_PATH]! performing curl to webhook...";
          $CURL "$WEBHOOK_URL" \
                -H 'Content-Type: application/json' \
                --data "{\"message\":\"SMART check failed!\",\"cluster\":\"$CLUSTER\",\"node\":\"$NODE\",\"disk\":\"$DISK_PATH\"}";
          exit 0;
      fi
  fi

  echo -e "Disk check passed!";

done < <(echo -e "$DISK_PATHS")
