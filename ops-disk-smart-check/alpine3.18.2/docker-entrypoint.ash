#!/usr/bin/env ash
set -e

while true; do
  TIME=$(date +%F@%H_%M_%S-%Z)
  echo -e "[echo] running smart check @ [$TIME]..."
  ash  /scripts/smart-check.ash
  echo -e "[echo] sleeping for 1 hour..."
  sleep 3600; # sleep
done;
