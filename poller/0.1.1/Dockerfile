# alpine:latest (amd64) as of 11/11/2021
FROM alpine@sha256:69704ef328d05a9f806b6b8502915e6a0a4faa4d72018dc42343f511490daf8a AS download

ARG POLLER_VERSION
ENV POLLER_VERSION=${POLLER_VERSION:-0.1.1}

RUN wget https://github.com/orndorffgrant/poller/archive/refs/tags/${POLLER_VERSION}.zip && \
    unzip ${POLLER_VERSION}.zip && \
    mv poller-${POLLER_VERSION} /poller

# rust:1.62.1-slim (amd64) as of 2022/07/23
FROM rust@sha256:d0c55188cf0f90af76a5872af0051375e0d1bcca032eacc6adec883b01f0081f AS build

# Install NodeJS
RUN apt update
RUN apt install -y curl nodejs npm unzip

COPY --from=download /poller /poller

WORKDIR /poller

# Build assets (for some reason this project uses deno *AND* nodejs...)
#
# Deno doesn't support alpine/CentOS7, so we can't use alpine.
# - https://github.com/denoland/deno/issues/9153
# - https://github.com/denoland/deno/issues/1658
RUN curl -fsSL https://deno.land/x/install/install.sh | sh
RUN chmod +x /root/.deno/bin/deno
RUN cp /root/.deno/bin/deno /bin/deno

# Build Frontend (NodeJS + Deno)
RUN cd assets-src && npm install && npm run build

# Build Backend (Rust)
RUN cargo build --release

# debian:bullseye-20220711-slim as of 2022/07/24
FROM debian@sha256:f52f9aebdd310d504e0995601346735bb14da077c5d014e9f14017dadc915fe5

COPY --from=build /poller/target/release/poller /bin/poller

VOLUME /data

EXPOSE 8000

CMD [ "/bin/poller", "run", "/data/poller.db" ]
