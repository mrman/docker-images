# Umami

Dockerized [`Umami`](https://umami.is) image that can be launched easily from docker.

At present (2022/11) there is no official `umami` image on DockerHub.
