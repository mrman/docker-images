# [Galene][galene]

Dockerized [`galene`](https://github.com/jech/galene) image, while there is not an official one.

[galene]: https://galene.org
