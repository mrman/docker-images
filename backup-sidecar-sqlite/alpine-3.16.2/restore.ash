#!/usr/bin/env ash
set -e

# Check provided ENV
if [ -z "$BACKUP_SCRATCH_DIR" ]; then
    BACKUP_SCRATCH_DIR=/tmp
fi

if [ -z "$SQLITE_DB_PATH" ]; then
    echo -e "SQLITE_DB_PATH not specified!" 1>&2;
    exit -1;
fi

if [ -z "$INTERVAL_SECONDS" ]; then
    INTERVAL_SECONDS=43200
fi

if [ -z "$BUCKET" ]; then
    BUCKET=backups
fi

if [ -z "$BUCKET_PREFIX" ]; then
    BUCKET_PREFIX=sqlite
fi

if [ -z "$B2_ACCOUNT_ID" ]; then
    echo -e "B2_ACCOUNT_ID not set"
    exit -1
fi

if [ -z "$B2_KEY" ]; then
    echo -e "B2_KEY not set"
    exit -1
fi

if [ -z "$BACKUP_NAME" ]; then
    echo -e "BACKUP_NAME must be set for a restore"
    exit -1
fi
echo -e "[info] BACKUP_NAME=${BACKUP_NAME}"

export COMPRESSED_BACKUP_NAME=${BACKUP_NAME}.tar.gz
echo -e "[info] COMPRESSED_BACKUP_NAME=${COMPRESSED_BACKUP_NAME}"

echo -e "[info] clearing the scratch directory..."
mkdir -p ${BACKUP_SCRATCH_DIR}
rm -rf ${BACKUP_SCRATCH_DIR}/*

export BACKUP_DIR=${BACKUP_SCRATCH_DIR}/${BACKUP_NAME}
echo -e "[info] BACKUP_DIR=${BACKUP_DIR}"

export BACKUP_FILE_PATH=${BACKUP_SCRATCH_DIR}/${COMPRESSED_BACKUP_NAME}
echo -e "[info] BACKUP_FILE_PATH=${BACKUP_FILE_PATH}"

export REMOTE_BACKUP_DIR=:b2:$BUCKET/${BUCKET_PREFIX}/${COMPRESSED_BACKUP_NAME}
echo -e "[info] retrieving SQLite backup from Backblaze under account [${B2_ACCOUNT_ID}]..."
echo -e "[info] retrieving backup [${REMOTE_BACKUP_DIR}]"
rclone copy \
  --b2-account $B2_ACCOUNT_ID \
  --b2-key $B2_KEY \
  ${REMOTE_BACKUP_DIR} \
  ${BACKUP_SCRATCH_DIR}

# Print backup size
export BACKUP_SIZE=$(du -hs ${BACKUP_FILE_PATH})
echo -e "[info] SQLite Backup size: [${BACKUP_SIZE}]"

# Unzip the backup
echo -e "[info] Unzipping SQLite backup..."
cd ${BACKUP_SCRATCH_DIR} && tar -xf ${COMPRESSED_BACKUP_NAME}

# Restore main SQLite DB
export SQLITE_DB_BACKUP_PATH=${BACKUP_SCRATCH_DIR}/${BACKUP_NAME}
echo "[info] restoring main SQLite backup from [${SQLITE_DB_BACKUP_PATH}]..."
sqlite3 ${SQLITE_DB_PATH} ".restore '${SQLITE_DB_BACKUP_PATH}'"
echo -e "[info] main SQLite DB backup restored from [${SQLITE_DB_BACKUP_PATH}]"

echo "[info] Restore completed from [${REMOTE_BACKUP_DIR}]"
