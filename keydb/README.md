# KeyDB

Dockerized [`KeyDB`](https://keydb.dev) image.

Improvements:
- Latest `ca-certificates` included (as of date of image build)

## Why not the official KeyDB standalone image?

SSL certs are purged from the container, so they need to be installed and this container needs to be updated consistently. The SSL certs installed should be enough to install Let's Encrypt certs.
