# Honkit

Dockerized [`Honkit`](https://github.com/honkit/honkit) image that can be launched easily from docker.

Note that this image is an improved version of the original image available which has a quite old version of Calibre, and does *not* work out of the box (as of 2023/11/19).

Compared to the original image, this image makes `honkit` the docker `ENTRYPOINT`, which means `docker` invocations that use `honkit` must look like the following: 

```console
docker run -v /some/path/to/your/book:/book -w /book --rm -it honkit pdf
```

`honkit` is the image name there, and `pdf` is the first argument to the default `ENTRYPOINT` (i.e. `honkit`).
