# Jitsi Jibri

A custom build of [Jitsi][jitsi]'s [`jibri`][jibri] compnent which makes it easy to use to write to different storage and as an event recorder.

Primarily this build of `jibri` adds `rclone` so that recordings can be sent out to object storage.
