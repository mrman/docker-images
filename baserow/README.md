# Baserow

Dockerized [`Baserow`](https://gitlab.com/bramw/baserow) image with none of premium features.

Baserow now has an [official docker image](https://hub.docker.com/r/baserow/baserow) that's fantastic! This repo should no longer be needed.

## Why not the official Baserow standalone image?

They just don't work for me. Addition of `caddy` is great, but it introduces needless complexity (in my setups, I have TLS terminated elsewhere).

The configuration for the ports seems to no longer be correct either, and listening on ports that *aren't externally exposed* seems to be the norm.

### (2022/09/23) The baserow images are good enough to use

As of now the official baserow images are good enough to use -- the internal `caddy` instance is there and fine.

The reasons against using the all-in-one image:

- Prescribed/built-in `redis`
- Prescribed/built-in `postgres`
