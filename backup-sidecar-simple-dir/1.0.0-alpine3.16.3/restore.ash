#!/usr/bin/env ash
set -e

# Check provided ENV
if [ -z "$BUCKET" ]; then
    echo -e "BUCKET must be specified"
    exit -1
fi

if [ -z "$BUCKET_PREFIX" ]; then
    echo -e "BUCKET_PREFIX must be specified"
    exit -1
fi

if [ -z "$BACKUP_NAME" ]; then
    echo -e "BACKUP_NAME must be set for a restore"
    exit -1
fi

if [ -z "$B2_ACCOUNT_ID" ]; then
    echo -e "B2_ACCOUNT_ID not set"
    exit -1
fi

if [ -z "$B2_KEY" ]; then
    echo -e "B2_KEY not set"
    exit -1
fi

if [ -z "$BACKUP_SCRATCH_DIR" ]; then
    BACKUP_SCRATCH_DIR=/tmp
fi

if [ -z "$INTERVAL_SECONDS" ]; then
    INTERVAL_SECONDS=43200
fi

echo -e "[info] BACKUP_NAME=${BACKUP_NAME}"

# Build the expected compressed backup name
export COMPRESSED_BACKUP_NAME=${BACKUP_NAME}.tar.gz
echo -e "[info] COMPRESSED_BACKUP_NAME=${COMPRESSED_BACKUP_NAME}"

# Prepare the scratch directory
echo -e "[info] clearing the scratch directory..."
mkdir -p ${BACKUP_SCRATCH_DIR}
rm -rf ${BACKUP_SCRATCH_DIR}/*

# Set up backup directory name
export BACKUP_DIR=${BACKUP_SCRATCH_DIR}/${BACKUP_NAME}
echo -e "[info] BACKUP_DIR=${BACKUP_DIR}"

# Set up path to compresse dbackup
export BACKUP_FILE_PATH=${BACKUP_SCRATCH_DIR}/${COMPRESSED_BACKUP_NAME}
echo -e "[info] BACKUP_FILE_PATH=${BACKUP_FILE_PATH}"

# Build and pull remote backup
export REMOTE_BACKUP_DIR=:b2:$BUCKET/${BUCKET_PREFIX}/${COMPRESSED_BACKUP_NAME}
echo -e "[info] retrieving Simple Directory backup from Backblaze under account [${B2_ACCOUNT_ID}]..."
echo -e "[info] retrieving backup [${REMOTE_BACKUP_DIR}]"
rclone copy \
  --b2-account $B2_ACCOUNT_ID \
  --b2-key $B2_KEY \
  ${REMOTE_BACKUP_DIR} \
  ${BACKUP_SCRATCH_DIR}

# Print backup size
export BACKUP_SIZE=$(du -hs ${BACKUP_FILE_PATH})
echo -e "[info] Simple Directory Backup size: [${BACKUP_SIZE}]"

# Unzip & restore the backed up files, which should be from /
# (i.e. saved with cp --parents or rsync -R)
echo -e "[info] Unzipping Simple Directory backup..."
cd ${BACKUP_SCRATCH_DIR} && tar -xvz -C / -f ${COMPRESSED_BACKUP_NAME} --strip-components=1

echo "[info] Restore completed from [${REMOTE_BACKUP_DIR}]"
