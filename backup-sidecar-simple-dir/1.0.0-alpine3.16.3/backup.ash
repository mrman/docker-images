#!/usr/bin/env ash
set -e

# Check provided ENV
if [ -z "$DATA_DIR" ]; then
    echo -e "DATA_DIR must be specified"
    exit -1
fi

if [ -z "$BUCKET" ]; then
    echo -e "BUCKET must be specified"
    exit -1
fi

if [ -z "$BUCKET_PREFIX" ]; then
    echo -e "BUCKET_PREFIX must be specified"
    exit -1
fi

if [ -z "$B2_ACCOUNT_ID" ]; then
    echo -e "B2_ACCOUNT_ID not set"
    exit -1
fi

if [ -z "$B2_KEY" ]; then
    echo -e "B2_KEY not set"
    exit -1
fi

if [ -z "$BACKUP_SCRATCH_DIR" ]; then
    BACKUP_SCRATCH_DIR=/tmp
fi

if [ -z "$INTERVAL_SECONDS" ]; then
    INTERVAL_SECONDS=43200
fi

if [ -z "$BACKUP_NAME_PREFIX" ]; then
    BACKUP_NAME_PREFIX=backup
fi

# Pre-create directories that will be used

BACKUP_NAME=${BACKUP_NAME_PREFIX}-`date +%F@%H_%M_%S-%Z`
echo -e "[info] BACKUP_NAME=${BACKUP_NAME}"

export COMPRESSED_BACKUP_NAME=${BACKUP_NAME}.tar.gz
echo -e "[info] COMPRESSED_BACKUP_NAME=${COMPRESSED_BACKUP_NAME}"

export BACKUP_DIR=${BACKUP_SCRATCH_DIR}/${BACKUP_NAME}
echo -e "[info] BACKUP_DIR=${BACKUP_DIR}"

export BACKUP_FILE_PATH=${BACKUP_SCRATCH_DIR}/${COMPRESSED_BACKUP_NAME}
echo -e "[info] BACKUP_FILE_PATH=${BACKUP_FILE_PATH}"

# Create backup dir
mkdir -p ${BACKUP_DIR}

# Copy the folder into the backup directory, preserving the path from /
rsync -r -R ${DATA_DIR} ${BACKUP_DIR}

# Print backup size
export BACKUP_SIZE=$(du -hs ${BACKUP_DIR})
echo -e "[info] Simple Directory Backup size: [${BACKUP_SIZE}]"

echo -e "[info] Zipping Simple Directory backup..."
cd ${BACKUP_SCRATCH_DIR} && tar -czf ${COMPRESSED_BACKUP_NAME} ${BACKUP_NAME}

export REMOTE_BACKUP_DIR=:b2:$BUCKET/${BUCKET_PREFIX}
echo -e "[info] saving Simple Directory backup to Backblaze under account [${B2_ACCOUNT_ID}]..."
echo -e "[info] saving backup to [${REMOTE_BACKUP_DIR}]"
rclone copy \
  --b2-account $B2_ACCOUNT_ID \
  --b2-key $B2_KEY \
  ${BACKUP_FILE_PATH} \
  ${REMOTE_BACKUP_DIR}

echo "[info] Backup completed, saved to [${REMOTE_BACKUP_DIR}/${COMPRESSED_BACKUP_NAME}]"
