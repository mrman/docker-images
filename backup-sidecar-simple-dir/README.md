# `backup-sidecar-simple-dir`

A sidecar container for running continuous backups on the filesystem (local disk), by simply recursively copying a directory.

NOTE: This sidecar should be deprecated in favor of simple `VolumeSnapshot` saving techniques on a k8s cluster, using tools like:

- [`FairwindsOps/gemini`](https://github.com/FairwindsOps/gemini)
- [`velero`](https://velero.io)

## Configuration

| ENV                                    | Default                          | Example                          | Description                                                                                        |
|----------------------------------------|----------------------------------|----------------------------------|----------------------------------------------------------------------------------------------------|
| `OPERATION`                            | `backup`                         | `restore`                        | Operation to perform with the sidecar container                                                    |
| `BACKUP_NAME_PREFIX`                   | `backup`                         | `my-special-backup`              | Prefix that will be used before date of backup                                                     |
| `BACKUP_NAME`                          | `backup-$(date +%F@%H_%M_%S-%Z)` | `backup-2021-10-13@13_47_28-UTC` | Name of the backup file (`.tar.gz` will be added as a suffix, `$BUCKET__PREFIX` will be prepended) |
| `DATA_DIR`                             | `/path/to/dir`                   | `/data`                          | Directories that should be copied                                                                  |
| `BACKUP_INTERVAL_SECONDS`              | `43200` (12h)                    | `86400` (24h)                    | Backup interval in seconds                                                                         |
| `BUCKET`                               | `backups`                        | `backups`                        | Top level bucket name                                                                              |
| `BUCKET_PREFIX`                        | `maddy/backups`                  | `your/dir/structure`             | Directory struture after bucket name, but before date (ex. `$BUCKET/$BUCKET_PREFIX/$BACKUP_NAME`)  |
| `B2_ACCOUNT_ID`                        | N/A                              | N/A                              | [Backblaze B2][backblaze-b2] Account ID                                                            |
| `B2_KEY`                               | N/A                              | N/A                              | [Backblaze B2][backblaze-b2] Key                                                                   |

[backblaze-b2]: https://www.backblaze.com/b2
